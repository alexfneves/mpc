clear;

%% system
[A, B, C, D] = tf2ss(1, [1 0 0]);
sys = ss(A, B, C, D);
Ts = 1;
sys = c2d(sys, Ts);
x0 = [0; 10];
N = 2;
[Nx, Nu] = size(B);
temp = size(C);
Ny = temp(1,1);

%% system info
sys.InputGroup.MV = 1;
sys.OutputGroup.MO = 1;
sys.InputName = {'accel'};
sys.OutputName = {'pos'};

%% creating the object
MPCobj = mpc(sys);
MPCobj.PredictionHorizon = N;
MPCobj.MV = struct('Min',-1,'Max',1);
%MPCobj.Weights.OV = 1;
%MPCobj.OV.Max = 10;
%MPCobj.OV.Min = -10;
%MPCobj.Weights.ManipulatedVariables = 1/10;
%MPCobj.Weights.OutputVariables = 1;





%% Bemporad

P = eye(Nx)*2;
Q = eye(Nx);
R = eye(Nu)/10;

Q_bar = zeros(N*Nx);
for i = 1:(N - 1)
    Q_bar((1 + (i - 1)*Nx):i*Nx, (1 + (i - 1)*Nx):i*Nx) = Q;
end
Q_bar((1 + (N - 1)*Nx):N*Nx, (1 + (N - 1)*Nx):N*Nx) = P;

R_bar = zeros(N*Nu);
for i = 1:N
    R_bar((1 + (i - 1)*Nu):i*Nu, (1 + (i - 1)*Nu):i*Nu) = R;
end

S_bar = zeros(N*Nx, N*Nu);
for i = 1:N
    temp = sys.B;
    for j = i:-1:1
        S_bar((1 + (i - 1)*Nx):i*Nx, (1 + (j - 1)*Nu):j*Nu) = temp;
        temp = sys.A*temp;
    end
end

T_bar = zeros(N*Nx, Nx);
temp = sys.A;
for i = 1:N
    T_bar((1 + (i - 1)*Nx):i*Nx, :) = temp;
    temp = sys.A*temp;
end

H = 2*(R_bar + S_bar'*Q_bar*S_bar);
F = 2*T_bar'*Q_bar*S_bar;
 
temp = (-inv(H)*(F'));
K = temp(1:Ny, 1:Nx);