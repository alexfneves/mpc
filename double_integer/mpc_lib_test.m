clear;

%% system
% A = [-0.0285 -0.0014; -0.0371 -0.1476];
% B = [-0.0850 0.0238; 0.0802 0.4462];
% C = [0 1; 1 0];
% D = zeros(2,2);
% sys = ss(A,B,C,D);
%[A, B, C, D] = tf2ss(1, [1 0 0]);
%sys = ss(A, B, C, D);
%Ts = 1;
%sys = c2d(sys, Ts);
sys = ss([1 1 ; 0 1], [0; 1] ,[1 0] , 0, 1);

%% system info
% sys.InputName = {'T_c','C_A_i'};
% sys.OutputName = {'T','C_A'};
% sys.StateName = {'C_A','T'};
sys.InputGroup.MV = 1;
% sys.InputGroup.UD = 2;
sys.OutputGroup.MO = 1;
% sys.OutputGroup.UO = 2;
sys.InputName = {'accel'};
sys.OutputName = {'pos'};

%old_status = mpcverbosity('off');

%% creating the object
Ts = 1;
MPCobj = mpc(sys,Ts);
MPCobj.PredictionHorizon = 2;
MPCobj.MV = struct('Min',-1,'Max',1);
%MPCobj.Weights.OV = 1;
%MPCobj.OV.Max = 10;
%MPCobj.OV.Min = -10;
MPCobj.Weights.ManipulatedVariables = 1/10;
MPCobj.Weights.OutputVariables = 1;
%MPCobj.MO = struct('Min',-10,'Max',10);
%MPCobj.MV.Min = -10;
%MPCobj.MV.Max = 10;
%MPCobj.MV.RateMin = -3;
%MPCobj.MV.RateMax = 3;
%review(MPCobj)

%% simulate
T = 26;
%r = [0 0; 2 0];
r = [0];
MPCopts = mpcsimopt;
MPCopts.PlantInitialState = [10; 0];
%MPCopts.Constraints = 'off';
sim(MPCobj,T,r,MPCopts);